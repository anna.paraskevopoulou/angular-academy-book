export class Student {
    public id: number;
    public first_name: string;
    public last_name: string;
    public DoB: string;
    public image: string;
    public studies: string[];
    public academy_period: string;
    public comments: string[];

    constructor(id: number, f_name: string, l_name: string, DoB: string, image: string, studies: string[], ac_period: string, comments: string[] ) {
        this.id = id;
        this.first_name = f_name;
        this.last_name = l_name;
        this.DoB = DoB;
        this.image = image;
        this.studies = studies;
        this.academy_period = ac_period;
        this.comments = comments;
    }
}
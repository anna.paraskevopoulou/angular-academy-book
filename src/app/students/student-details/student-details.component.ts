import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/internal/Subscription';
import { Student } from '../student.model';
import { StudentService } from '../students.service';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.scss']
})
export class StudentDetailsComponent implements OnInit, OnDestroy {

  public currentStudent: Student;
  paramsSubscription: Subscription;

  constructor(
    public _studentService: StudentService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.currentStudent = this._studentService.serviceStudent;
    
    // this.paramsSubscription = this.route.params.subscribe(
    //   (params: Params) => {
    //     this.currentStudent = {
    //       id: params['id'],
    //       first_name: '',
    //       last_name: '',
    //       DoB: null,
    //       image: null,
    //       studies: null,
    //       academy_period: null,
    //       comments: null
    //     };
    //   })
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
  }

}

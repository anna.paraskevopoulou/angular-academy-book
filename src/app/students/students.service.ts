import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Student } from './student.model';

@Injectable()
export class StudentService {

  constructor(private http: HttpClient) {   }

  serviceStudent: Student;
  studentUrl = '../assets/students.json';

  getStudents() {
    return this.http.get(this.studentUrl);
  }

}
import { Component, OnInit } from '@angular/core';
import { Student } from '../student.model';
import { StudentService } from '../students.service';
import students from '../../../assets/students.json';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-page',
  templateUrl: './student-page.component.html',
  styleUrls: ['./student-page.component.scss']
})
export class StudentPageComponent implements OnInit {

  studentsArray: Student[] = [];

  constructor(
    public _studentService: StudentService,
    private router: Router) {
  }

  ngOnInit(): void {
    console.log(students);
    this.studentsArray = students.students;
  }

  public onStudentClicked(student: Student) {
    console.log(student);
    this._studentService.serviceStudent = student;
    this.router.navigate(['/student-details']);
  }
   
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentDetailsComponent } from './students/student-details/student-details.component';
import { StudentPageComponent } from './students/student-page/student-page.component';

const routes: Routes = [
  { path: '', component: StudentPageComponent },
  { path: 'student-page', component: StudentPageComponent },
  { path: 'student-details', component: StudentDetailsComponent },
  { path: 'student-page/:id', component: StudentDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
